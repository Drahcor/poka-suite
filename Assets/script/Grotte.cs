﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grotte : MonoBehaviour
{

    public GameObject stalactique;
    public float mass;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
           Rigidbody stalactiqueRB= stalactique.AddComponent<Rigidbody>();
            stalactiqueRB.mass = mass;
        }
        
    }
}
