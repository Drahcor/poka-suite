﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;

public class BoutonTeleporteur : MonoBehaviour{

    public TeleportController teleControl;
    private GameManager gameManager;
    
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            teleControl.Teleportation();
            gameManager.target = gameManager.player.transform.position;
        }
    }
}