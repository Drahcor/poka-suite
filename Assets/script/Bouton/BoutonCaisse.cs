﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BoutonCaisse : MonoBehaviour
{
    private GameManager gameManager;

    GameObject player;
    public GameObject caisse;

    public float force;

    Rigidbody caisseRB;
    Rigidbody playerRB;

    LayerMask bouton;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerRB = player.GetComponent<Rigidbody>();
        
        caisse = transform.parent.gameObject;
        caisseRB = transform.parent.GetComponentInParent<Rigidbody>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
    }

    private void OnMouseOver()
    {
        gameManager.bloque = true;
        
        if (Input.GetMouseButtonDown(0))
        {
            if (gameObject.CompareTag("boutonMonter"))
                player.transform.position = caisse.gameObject.transform.position + new Vector3(-1, 2f, 0);
        }

        if (Input.GetMouseButton(0))
        {
            if (gameObject.CompareTag("BoutonLeft"))
            {
                Debug.Log("BoutonLeft");
                caisseRB.transform.Translate(Vector3.left * Time.deltaTime * force);
                playerRB.transform.Translate(Vector3.left * Time.deltaTime * force);
            }

            else if (gameObject.CompareTag("BoutonRight"))
            {
                caisseRB.transform.Translate(Vector3.right * Time.deltaTime * force);
                playerRB.transform.Translate(Vector3.right * Time.deltaTime * force);
            }
        }
    }

    private void OnMouseExit()
    {
        gameManager.bloque = false;
    }
}
