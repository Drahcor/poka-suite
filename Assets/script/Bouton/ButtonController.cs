﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class ButtonController : MonoBehaviour{

    private GameManager gameManager;
    private InterrupteurController interrupteur;

    GameObject player;
//    public GameObject caisse;

    public float force = 5;
    public float distanceMax;

//    Rigidbody caisseRB;
    Rigidbody playerRB;
    
    GameObject Lever;
    GameObject Baisser;

    private GameObject porteButton;
    public float portePositionDepart;
    AudioSource audioSourceDoor;

    LayerMask bouton;

    private TriggerPoulie triggerPoulie;
    private float detectionTrous;
    public float ZduPlayer;

    void Start()
    {
        
        player = GameObject.FindGameObjectWithTag("Player");
        playerRB = player.GetComponent<Rigidbody>();

        if (GameObject.FindWithTag("BoutonInterrupteur") != null)
        {
            interrupteur = transform.parent.parent.gameObject.GetComponent<InterrupteurController>();
            porteButton = interrupteur.porte;
            portePositionDepart = porteButton.transform.position.y;
            audioSourceDoor = transform.parent.parent.gameObject.GetComponent<AudioSource>();
        }

//        caisse = transform.parent.parent.gameObject;
//        caisseRB = caisse.GetComponent<Rigidbody>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        ZduPlayer = player.transform.position.z;
        
        if (GameObject.FindWithTag("triggerPoulie"))
        {
            triggerPoulie = transform.parent.parent.gameObject.GetComponent<TriggerPoulie>();
        }
    }

    void Update()
    { 
    }

    private void OnMouseOver()
    {
        // CAISSE BOUTTON
        if (Input.GetMouseButton(0))
        {
            gameManager.bloque = true;
            
//            if (gameObject.CompareTag("boutonMonter"))
//            {
//                player.transform.position = caisse.gameObject.transform.position + new Vector3(0, 2f, 0);
//                gameManager.bloque = false;
//            }
//                
//            if (gameObject.CompareTag("BoutonLeft"))
//            {
//                if (player.transform.position.x < caisse.transform.position.x && gameManager.tourneVersLaGauche)
//                {
//                    player.transform.rotation = Quaternion.Euler(0, 0, 0);
//                    gameManager.tourneVersLaGauche = false;
//                }
//                
//                else if (player.transform.position.x > caisse.transform.position.x && !gameManager.tourneVersLaGauche)
//                {
//                    player.transform.rotation = Quaternion.Euler(0, 180, 0);
//                    gameManager.tourneVersLaGauche = true;
//                }
//
//                int direction = (gameManager.tourneVersLaGauche) ? 1 : -1;
//                
//                caisseRB.transform.Translate(new Vector3(-1,0,0) * Time.deltaTime * force);
//                playerRB.transform.Translate(new Vector3(direction,0,0) * Time.deltaTime * force);
//            }
//
//            else if (gameObject.CompareTag("BoutonRight"))
//            {
//                if (player.transform.position.x > caisse.transform.position.x && !gameManager.tourneVersLaGauche)
//                {
//                    player.transform.rotation = Quaternion.Euler(0, 180, 0);
//                    gameManager.tourneVersLaGauche = true;
//                      
//                }
//                
//                else if (player.transform.position.x < caisse.transform.position.x && gameManager.tourneVersLaGauche)
//                {
//                    player.transform.rotation = Quaternion.Euler(0, 0, 0);
//                    gameManager.tourneVersLaGauche = false;
//                }
//
//                int direction = (gameManager.tourneVersLaGauche) ? -1 : 1;
//                
//                caisseRB.transform.Translate(new Vector3(1,0,0) * Time.deltaTime * force);
//                playerRB.transform.Translate(new Vector3(direction,0,0) * Time.deltaTime * force);
//            }

            if (interrupteur != null)
            {
                if (gameObject.CompareTag("BoutonInterrupteur"))
                {
                    gameManager.bloque = true;
                
                    if (porteButton.transform.position.y >= portePositionDepart + 3f)
                    {
                        porteButton.transform.position = porteButton.transform.position;
                        audioSourceDoor.Play(0);
                    }
                    else
                    {
                        porteButton.transform.Translate(Vector3.up * Time.deltaTime);
                    }
                }
            }

            
            Ray derriereRay = new Ray(player.transform.position, -gameManager.sensDesRaycast); // Raycast tirer derriere le player 
            Ray trousRay = new Ray(new Vector3(player.transform.position.x +1,player.transform.position.y,player.transform.position.z), -Vector3.up); // tirer du player vers le bas
            RaycastHit bloqueHit;
            Debug.DrawRay(player.transform.position, -gameManager.sensDesRaycast,Color.cyan);
            Debug.DrawRay(new Vector3(player.transform.position.x +1,player.transform.position.y,player.transform.position.z), -Vector3.up);
               
            //Raycast tirer dans le dos du player pour détecter si il y a un mur derriere lui
            if (Physics.Raycast(derriereRay, out bloqueHit, 1f, gameManager.groundLayer))
            {
                Debug.Log("dos au mur");
            }

            // Raycast tirer devant le player 
            if (Physics.Raycast(trousRay, out bloqueHit, detectionTrous, gameManager.groundLayer))
            {
                if (detectionTrous >= 2)
                {
                    Debug.Log("trous");
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (gameObject.CompareTag("boutonMonterAscenseur"))
            {
                if (triggerPoulie.clone != null)
                {
                    triggerPoulie.clone.SetActive(false);
                }
                Vector3 posA = triggerPoulie.startPos.position;
                Vector3 posB = triggerPoulie.endPos.position;

                triggerPoulie.ascenseur = transform.parent.parent.parent.parent.gameObject;
                gameManager.player.transform.parent = triggerPoulie.ascenseur.transform;
                
                triggerPoulie.startTime = Time.time;
                gameManager.player.transform.position = new Vector3(gameManager.player.transform.position.x,gameManager.player.transform.position.y,ZduPlayer);
                if (triggerPoulie.activerAscenseur)
                {
                // rejoins la position B
                    triggerPoulie.totalDistance = Vector3.Distance(posA, posB);
                    triggerPoulie.ascenseur.transform.position = Vector3.Lerp(triggerPoulie.startPos.position, triggerPoulie.endPos.position, triggerPoulie.journeyFraction);
                    triggerPoulie.activerAscenseur = false;
                    triggerPoulie.retournerPosition = true;
                }
               else if (triggerPoulie.retournerPosition)
                { 
                // rejoins la position A
                    triggerPoulie.totalDistance = Vector3.Distance(posB,posA);
                    triggerPoulie.activerAscenseur = true;
                    triggerPoulie.retournerPosition = false;
                    triggerPoulie.ascenseur.transform.position = Vector3.Lerp(triggerPoulie.endPos.position,triggerPoulie.startPos.position, triggerPoulie.journeyFraction);
                }
            }
        }
    }

    private void OnMouseExit()
    {
        // regler le probleme car se balade dans l'ascenseur
        gameManager.bloque = false;
        gameManager.player.transform.position = new Vector3(gameManager.player.transform.position.x,gameManager.player.transform.position.y,ZduPlayer);
    }
}
