﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barreaux : MonoBehaviour{

    public GameObject barreaux;
    public float MonterLever;

    void Start()
    {
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            barreaux.transform.Translate(Vector3.up * MonterLever);
            gameObject.SetActive(false);
        }

    }

    private void OnTriggerExit(Collider other)
    {

    }
}
