﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour{

    GameObject player;
    private Scene scene;
    Checkpoint gameManager;
 
    void Start()
    {
        scene = SceneManager.GetActiveScene();
        gameManager = GameObject.FindWithTag("spawner").GetComponent<Checkpoint>();
        player = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag =="Player")
        {
            gameManager.TeleportSpawn();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag =="Player")
        {
            gameManager.TeleportSpawn();
        }
    }
}
