﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPoulie : MonoBehaviour
{
    private ButtonController boutonControler;
    
    private GameManager gameManager;
    public GameObject BoutonPoulie;
    public GameObject clone;
    public bool activerAscenseur;
    public bool retournerPosition;
    public bool valTrigger; // vérifie que le Player est bien dans le trigger de la mannivelle
    public GameObject ascenseur;
    public Transform startPos, endPos;
    public float startTime, totalDistance;
    public float speedAscenseur;
    public float journeyFraction;

    private GameObject arrowUp;
    private GameObject arrowDown;


    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        clone = null;

        if (GameObject.FindWithTag("bouton") != null)
        {
            boutonControler = GameObject.FindWithTag("bouton").GetComponent<ButtonController>();
        }
    }
    
    void Update()
    {
        if (clone != null && startPos.transform.position.y < endPos.transform.position.y)
        {
            arrowUp = transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            arrowDown = transform.GetChild(0).GetChild(0).GetChild(1).gameObject;
        }
        else if (clone != null && startPos.transform.position.y > endPos.transform.position.y)
        {
            arrowDown = transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            arrowUp = transform.GetChild(0).GetChild(0).GetChild(1).gameObject;
        }

        Ascenseur();

        PositionPlateforme();
    }

    private void OnTriggerEnter(Collider other)
    {
        valTrigger = true;

        ascenseur = transform.parent.parent.gameObject;
        
        if (other.gameObject.CompareTag(("Player")) && clone == null)
        {
            clone = Instantiate(BoutonPoulie, transform.position, Quaternion.identity);
            clone.transform.parent = transform;
            clone.transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z-1.5f);
        }
        else if (other.gameObject.CompareTag(("Player")) && clone != null)
        {
            clone.SetActive(true);
        }

        if (ascenseur.transform.position != endPos.transform.position)
        {
            activerAscenseur = false;
            retournerPosition = true;
        }
        else
        {
            activerAscenseur = true;
            retournerPosition = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        valTrigger = false;
        
        if (other.gameObject.CompareTag("Player"))
        {
            clone.SetActive(false);
            gameManager.player.transform.parent = null;
        }
    }

    private void Ascenseur()
    {
        float currentduration = (Time.time - startTime) * speedAscenseur;  // cb de temps pour aller du point a au b
        float journeyFraction = currentduration / totalDistance; // recalcul la valeur du currentduration en boucle
        
        if (activerAscenseur)
        {
            ascenseur.transform.position = Vector3.Lerp(startPos.position, endPos.position, journeyFraction);
        }

        else if (retournerPosition)
        {
            ascenseur.transform.position = Vector3.Lerp(endPos.position,startPos.position, journeyFraction);
        }
    }

    private void PositionPlateforme()
    {
        if (ascenseur.transform.position == startPos.transform.position && clone != null) // si l'ascenseur est en bas
        {
            if (GameObject.FindWithTag("boutonMonterAscenseur") != null)
            {
                arrowDown.GetComponent<SpriteRenderer>().enabled = false;
                arrowUp.GetComponent<SpriteRenderer>().enabled = true;
            }
        }
        else if(ascenseur.transform.position == endPos.transform.position && clone != null) // si l'ascenseur est en haut
        {
            if (GameObject.FindWithTag("boutonMonterAscenseur") != null)
            {
                arrowDown.GetComponent<SpriteRenderer>().enabled = true;
                arrowUp.GetComponent<SpriteRenderer>().enabled = false;
            }
        }

        if (ascenseur.transform.position.y == startPos.transform.position.y && valTrigger)
        {
            if (clone != null)
            {
                clone.SetActive(true);
                gameManager.bloque = false;
            }
        }
        else if (ascenseur.transform.position.y == endPos.transform.position.y && valTrigger)
        {
            if (clone != null)
            {
                clone.SetActive(true);
                gameManager.bloque = false;
            }
        }
        else
        {
            if(clone != null)
            clone.SetActive(false);
        }
    }

}

