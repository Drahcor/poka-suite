﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoutonPoulie: MonoBehaviour
{
    GameManager gameManager;
    public TriggerPoulie poulie;

     GameObject Lever;
     GameObject Baisser;

    GameObject player;

    public float distanceMax;
    
    public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        gameManager = player.GetComponent<GameManager>();

    }

    void Update()
    {
        if (distanceMax >= 100)
        {
            Lever.transform.position = Lever.transform.position;
            Baisser.transform.position = Baisser.transform.position;
        }
    }

    private void OnMouseOver()
    {

        if (Input.GetMouseButton(0))
        {
            
            if (gameObject.CompareTag("BoutonLeft"))
            { 
                Lever.transform.Translate(Vector3.up * Time.deltaTime * distanceMax);
                Baisser.transform.Translate(Vector3.down * Time.deltaTime * 3);                
            }

             if (gameObject.CompareTag("BoutonRight"))
            {
                Lever.transform.Translate(Vector3.down * Time.deltaTime * 3);
                Baisser.transform.Translate(Vector3.up * Time.deltaTime * 3);
            }
        }
    }
    private void OnMouseExit()
    {
    }
}
