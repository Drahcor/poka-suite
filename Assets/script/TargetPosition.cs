﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPosition : MonoBehaviour
{
    private GameObject player;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (player.transform.position.x <= transform.position.x + 0.5 &&
            player.transform.position.x >= transform.position.x + 0.5)
        {
            Destroy(this);
        }
    }
}
