﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    private Rigidbody playerRB;
    
    public bool bloque; // bloqué poka (pour les boutons)
    public bool peutBouger;
    public bool tourneVersLaGauche;// qui détermine dans quel sens est tourné le player
    private float distanceSaut;
    public Vector3 sensDesRaycast; // définit que les raycast doivent tournés avec le Player

    public bool isJumping;
    private float timerJump;
    
    public LayerMask clickMask; // layer du quad
    public LayerMask groundLayer; //layer du sol
    public LayerMask caisseLayer; // layer d'une caisse
    public LayerMask boutonLayer; // layer des boutons
    public LayerMask Obstacle;


    public Vector3 target; // point toucher sur le quad
    public GameObject pointMouse;//objet référence
    private GameObject targetPointMouse; // objet instancé
    private bool touch; // définit état "toucher une fois" du raycast 

    private GameObject cubeRayDistanceSaut; //enfant du player qui calcul la prododeur des trous
    private float maxdistanceSol = 1; // valleur maximal que poka peut sauter

    public int speed;
    public float distanceGrounded = 1f  ;
    public float stepHeightMax;

    private GameObject caisse;
    private Rigidbody caisseRB;
    public bool clickCaisse; // booléan ce mettant sur true quand une caisse est toucher par la souris
    
    private GameObject clone;
    public GameObject boutonCaisse;

    public Camera camera;
    public Vector3 offsetHauteur;
    private Vector3 plafondOffset;
//    public Vector3 startPosCamera, endPosCamera;
    private float startTime;
    private float totalDistance ;

    private float positionX;
    public float cameraRotationX;

    // pour l'animation : definit que le player est bien en train de se déplacer
    public bool isMoving;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerRB = player.GetComponent<Rigidbody>();

        cubeRayDistanceSaut = player.transform.GetChild(0).gameObject;
    
        caisse = GameObject.FindGameObjectWithTag("caisse");
        
        if(caisse != null)
            caisseRB = caisse.GetComponent<Rigidbody>();

        camera.transform.position = player.transform.position + offsetHauteur;
    }

    void Update()
    {
        playerRB.constraints = RigidbodyConstraints.FreezeRotation;
        //Inputs
        //A besoin de bouger
        //Verification environnement
        //Deplacements

        camera.transform.rotation = Quaternion.Euler(cameraRotationX, camera.transform.rotation.y,camera.transform.rotation.z);
        
        if (!tourneVersLaGauche)
        {
            positionX = 0.8f;
        }
        else
        {
            positionX = -0.8f;
        }

        if (bloque == false)
        {
            DefinitionTarget();
            DetectionObstacles();
            
            TestSaut();
        }
 
        DeplacementPlayer();

        CaisseTest();

        MoveCamera();
        
        //vérifie que Poka n'a pas atteint sa targetPoint
        if (targetPointMouse != null && player.transform.position.x != targetPointMouse.transform.position.x)
        {
            isMoving = true;
        }
        else if(targetPointMouse == null ) // il est a la targetPoint ou il n'y a pas de targetPoint
        {
            isMoving = false;
        }
        else if (player.transform.position.x == targetPointMouse.transform.position.x)
        {
            isMoving = false;
        }
    }
    
    private void DefinitionTarget()
    {
        if (Input.GetMouseButtonDown(0) && peutBouger)
        {
            clickCaisse = false;
            
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition); // raycast tirer par la caméra vers le point cliqué
            RaycastHit CameraHit; // hit = raycast touche quelque chose
        
            if (Physics.Raycast(cameraRay, out CameraHit, 100f, clickMask)) // le raycast touche le clickmask
            {
                target = CameraHit.point; // la target est point toucher sur le quad
                target.y = target.y + 2.5f;
                
                //float directionY = player.GetComponent<CapsuleCollider>().bounds.min.y - target.y;  //calcul pour savoir si le l'endroit cliqué est au dessus ou en dessous du player                
                //Vector3 direction = new Vector3(0,directionY,0);  //direction = valeur minimum de son collider - position de la cible
                
                Vector3 direction = -Vector3.up;
                
                Ray targetRay = new Ray(new Vector3(target.x,target.y,target.z),direction ); // raycast tirer depuis la target vers le sol
                RaycastHit targetHit; // raycast touche quelque chose
                
                if (Physics.Raycast(targetRay, out targetHit, 1000f, groundLayer)) // le raycast touche le sol
                {
                    touch = true;
                    if (target != null)
                    {
                        Destroy(targetPointMouse);
                    }
                    
                    var position = player.transform.position;
                    targetPointMouse = Instantiate(pointMouse, new Vector3 (target.x, targetHit.point.y + 0.1f, position.z), Quaternion.Euler(90,0,0));

                    RotationPlayer();
                    bloque = false;
                }
            } 
        }
    }
    
    private void RotationPlayer()
    {
        if (targetPointMouse.transform.position.x < player.transform.position.x)
        {
            player.transform.rotation = Quaternion.Euler(0,0,0); // player a gauche, se tourne vers la droite
            tourneVersLaGauche = true;
            distanceSaut = -2.5f;
        }
        else
        {
            player.transform.rotation = Quaternion.Euler(0,180,0); // player a droite, se tourne vers la gauche
            tourneVersLaGauche = false;
            distanceSaut = 2.5f;
        }
        
        // vérifie dans quel sens est tourné le player pour que les raycast tourne avec lui
        if (!tourneVersLaGauche)
        {
            sensDesRaycast = new Vector3(1,0,0);
        }

        else if (tourneVersLaGauche)
        {
            sensDesRaycast = new Vector3(-1,0,0);
        }
    }
    
    private void DetectionObstacles()
    {
        //GROUND
        RaycastHit hit;
        Vector3 origin;
        Vector3 dir;

        origin = player.transform.position;
        dir = -transform.up;
        float widthPlayer = player.GetComponent<CapsuleCollider>().bounds.size.x /2; // largeur du player. size = taille du joueur. x = largeur.
        
        if (Physics.SphereCast(origin, widthPlayer, dir, out hit, distanceGrounded, groundLayer)) // le player est sur le sol
        {
            peutBouger = true;
        }
        
        //TEST SOLIDS UP
        // Obstacle à hauteur de tete
        Ray obstacleRay = new Ray(new Vector3(player.transform.position.x,player.transform.position.y +1.2f,player.transform.position.z),sensDesRaycast);
        RaycastHit bloqueHit;
        Debug.DrawRay(new Vector3(player.transform.position.x,player.transform.position.y +1.2f,player.transform.position.z),sensDesRaycast, Color.red);
        
        if (Physics.Raycast(obstacleRay, out bloqueHit,1.5f,Obstacle))
        {
            print("obstacle haut");
            target = player.transform.position; // bloque le player
            
        }
//        if (Physics.Raycast(obstacleRay, out bloqueHit, 0.5f,caisseLayer) && clickCaisse == false) // raycast détecte caisse mais poka passe au travers
//        {
//            SautCaisse();
//        }

        if (Physics.Raycast(obstacleRay, out bloqueHit, 1f, caisseLayer) /*&& clickCaisse*/) // raycast détecte caisse mais poka ne passe pas au travers
        {
            target = player.transform.position; // bloque le player
            clickCaisse = false;

            caisse = bloqueHit.collider.gameObject;

//            if (!clone)
//            {
//                clone = Instantiate(boutonCaisse, caisse.transform.position,Quaternion.identity);
//                clone.transform.parent = caisse.transform;
//                clone.transform.position = new Vector3(caisse.transform.position.x, caisse.transform.position.y+2, caisse.transform.position.z);
//            }
        }
        else
        {
            Destroy(clone);
        }

        //TEST SOLIDS DOWN
        // Obstacle marche à grimper
        Ray stairRay = new Ray(new Vector3(player.transform.position.x,player.GetComponent<CapsuleCollider>().bounds.min.y + stepHeightMax,player.transform.position.z),sensDesRaycast );
        RaycastHit stairHit;
        Debug.DrawRay(new Vector3(player.transform.position.x,player.GetComponent<CapsuleCollider>().bounds.min.y + stepHeightMax,player.transform.position.z),sensDesRaycast, Color.magenta);

        if (Physics.Raycast(stairRay, out stairHit,0.1f, groundLayer))
        {
            print("obstacle marche");
            target = player.transform.position; // bloque le player
        }
    }
    
//    private void SautCaisse()
//    {
//        if (player.transform.position.x > caisse.transform.position.x)
//        {
//            player.transform.position += new Vector3(-4f,0,0);
//        }
//        else
//        {
//            player.transform.position += new Vector3( 4f,0,0);
//        }
//    }
 
    private void DeplacementPlayer()
    {
        if (bloque)
        {
            target = player.transform.position;
        }
        // déplacement de Poka
        if ( bloque == false && touch) 
        {
            player.transform.position = Vector3.MoveTowards(player.transform.position, new Vector3(target.x, player.transform.position.y, player.transform.position.z), speed * Time.deltaTime);
        }
        
        //SAUT
        if (isJumping)
        {
            peutBouger = false;
            timerJump -= Time.deltaTime;

            if (timerJump > 0f)
            {
                if (tourneVersLaGauche) // trouve une plateforme, saute a gauche
                {
                    playerRB.velocity = new Vector3(-2, 10 * 5 * Time.deltaTime, 0);
                }
                else // trouve une plateforme, saute a droite
                {
                    playerRB.velocity = new Vector3(2, 10 * 5 * Time.deltaTime, 0);
                }
            }
            else
            {
                isJumping = false;
            }
        }
    }
    private void OnDrawGizmos()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        float widthPlayer = player.GetComponent<CapsuleCollider>().bounds.size.x;
            
        Gizmos.color = Color.red;
//        Gizmos.DrawSphere(new Vector3(player.transform.position.x,player.transform.position.y - widthPlayer,player.transform.position.z),player.GetComponent<CapsuleCollider>().bounds.size.x /2);
    }
    
    private void TestSaut()
    {
        // système de saut 
        Ray profondeurRay = new Ray( new Vector3(cubeRayDistanceSaut.transform.position.x, cubeRayDistanceSaut.transform.position.y,cubeRayDistanceSaut.transform.position.z),-transform.up); // raycast depuis enfant du player
        Debug.DrawRay(new Vector3(cubeRayDistanceSaut.transform.position.x, cubeRayDistanceSaut.transform.position.y,cubeRayDistanceSaut.transform.position.z),-transform.up ,Color.red);
        RaycastHit profondeurHit;
        if (Physics.Raycast(profondeurRay, out profondeurHit)) // détection du sol par le raycast
        {
            float profondeur = maxdistanceSol - profondeurHit.distance; // profondeur = distance autoriser - distance définit par le raycast qui sépare l'enfant du sol. sautHit = 1.4
            
            if (profondeur < -5) // si un trous est plus profond que
            {   
                Debug.Log("trous profond");
                    // créé un raycast qui cherche un obstacle devant le joueur
                    Ray testSautRay = new Ray(new Vector3(player.transform.position.x,player.transform.position.y ,player.transform.position.z),sensDesRaycast);
                    RaycastHit testSautHit;
                    Debug.DrawRay(new Vector3(player.transform.position.x,player.transform.position.y ,player.transform.position.z),sensDesRaycast, Color.yellow);
                
                if (Physics.Raycast(testSautRay,out testSautHit, 3f,Obstacle)) // si le raycast détecte un obstacle devant le trous
                {
                    Debug.Log("obstacle devant trous");
                    target = player.transform.position; // bloque le player
                }
                else // si il n'y a pas d'obstacles devant le trous
                {
                    //créer un second raycast qui cherche si il y a une plateforme
                    Ray cherchePlateformeRay = new Ray(new Vector3(player.transform.position.x + distanceSaut,player.transform.position.y + 1f ,player.transform.position.z),-transform.up );
                    RaycastHit cherchePlateformeHit;
                    Debug.DrawRay(new Vector3(player.transform.position.x + distanceSaut, player.transform.position.y + 1f, player.transform.position.z), -transform.up);
                    Debug.Log("ne saute pas !");

                    if (Physics.Raycast(cherchePlateformeRay, out cherchePlateformeHit, 3f))
                    {
                        if (cherchePlateformeHit.collider.gameObject != null) // si détecte quelque chose
                        {
                            isJumping = true;
                            timerJump = 0.2f;

                            if(tourneVersLaGauche)
                            {
                                target = new Vector3(player.transform.position.x - 2, target.y, target.z);
                            } 
                            
                            else
                            {
                                target = new Vector3(player.transform.position.x + 2, target.y, target.z);
                            }
                        }
                        else if(cherchePlateformeHit.collider.gameObject == null)// si il ne détecte pas de plateforme
                        {
                            target = player.transform.position;
                        }
                    }
                    else
                    {
                        target = player.transform.position;
                    }
                }
            }
            if (targetPointMouse != null)
            {
                if (profondeur > -4 && targetPointMouse.transform.position.y < player.transform.position.y - 0.55f) // si trous moins profond que
                {
                    print("trous pas très profond");
                }
            }
        }
    }
    
    RaycastHit CameraHit; // hit = raycast touche quelque chose

    private void CaisseTest()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition); // raycast tirer par la caméra vers le point cliqué
            
            if (Physics.Raycast(cameraRay, out CameraHit, 100f, caisseLayer))
            {
                clickCaisse = true;
            }
        }
    }

    public float positionYCamera;
    public float distancePlafond;
    
    public Vector3 grotteOffset = new Vector3(0f,3f,-6f);
    public Vector3 standartOffset = new Vector3(0f,5f,-9f);

    public float grotteRotation = 15;
    public float standartRotation =30;

    public float smoothCamera = 0.05f;

    private void MoveCamera()
    {
//        startPosCamera = camera.transform.position;
//        endPosCamera = new Vector3(player.transform.position.x,positionYCamera,startPosCamera.z);
        
//        totalDistance = Vector3.Distance(startPosCamera, endPosCamera);
        
//        camera.transform.position = player.transform.position + offsetHauteur;
        
//        float currentduration = (Time.time - startTime) * speed;
//        float journeyFraction = currentduration / totalDistance ;

        Ray plafondRay = new Ray(player.transform.position,Vector3.up);
        RaycastHit plafondHit;
        
        if (Physics.Raycast(plafondRay, out plafondHit, 4,groundLayer))
        {
            distancePlafond = player.transform.position.z - plafondHit.distance;
            
            if (distancePlafond <= -2)
            {
                offsetHauteur = grotteOffset;
                cameraRotationX = grotteRotation;

                Debug.Log("plafond");
//              positionYCamera = camera.transform.position.y - 2f;
//              camera.transform.position = Vector3.Lerp(startPosCamera,endPosCamera ,3 * Time.deltaTime);
//              camera.transform.rotation = Quaternion.Euler(20, 0, 0);
            }
            else
            {
                offsetHauteur = standartOffset;
                cameraRotationX = standartRotation;
            }
        }
        else
        {
            offsetHauteur = standartOffset;
            cameraRotationX = standartRotation;
        }
        
        Vector3 destination = new Vector3(player.transform.position.x + offsetHauteur.x,offsetHauteur.y ,offsetHauteur.z);
        Quaternion cameraRotation = Quaternion.Euler(cameraRotationX, camera.transform.rotation.y,camera.transform.rotation.z);
        
        camera.transform.position = Vector3.Lerp(camera.transform.position, destination ,smoothCamera);
        camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, cameraRotation, smoothCamera);
    }
}
