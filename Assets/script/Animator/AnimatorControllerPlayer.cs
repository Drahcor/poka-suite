﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorControllerPlayer : MonoBehaviour
{
    private GameManager gameManager;
    private Animator animator;
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.isMoving)
        {
            animator.SetBool("Deplacement",true);
        }
        else
        {
            animator.SetBool("Deplacement",false);
        }

        if (gameManager.isMoving)
        {
            GetComponent<Animator>().Play("Running");
        }
    }
}
