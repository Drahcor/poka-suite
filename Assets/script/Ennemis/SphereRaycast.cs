﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereRaycast : MonoBehaviour{

    PlayerController playControl;

    public float speed;
    private float speedStart;
    public float waitTime;
    public float startWaitTime;

    public Transform[] destination;
    private int destinationIndex;
    private int chemin;
    public float distancespot = 1f;

    public bool sentinnelalerte;
    public bool isWaiting;
    public bool searchPlayer;
    

    public float AlerteTimer = 0f;

    GameObject player;

    public GameObject[] ennemisAlerte;

    //NEW VARIABLE
    public Vector3 lastPos;
    public Vector3 posA;
    public Vector3 posB;
    public float dist;
    public float speedtest;

    private float startTime;
    public float distBetweenAB;

    void Start()
    {
        waitTime = startWaitTime;
        destinationIndex = chemin;
        startTime = Time.time;

        player = GameObject.FindWithTag("Player");
        playControl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    void Update()
    {

        if (sentinnelalerte && playControl.cacher)
        {
            isWaiting = true;
            sentinnelalerte = false;
        }
       DeplacementSphere();
       Attente();
}

    private void AppelEnnemis()
    {
        if (sentinnelalerte)
        {
            for (int i = 0; i < ennemisAlerte.Length; i++)
            {
                var speedEnnemi = ennemisAlerte[i].GetComponent<EnnemisGardes>().speed *2;
                ennemisAlerte[i].GetComponent<EnnemisGardes>().gardeAlerte = true;
            } 
        }
    }
    
    private void DeplacementSphere()
    {
        if (Vector3.Distance(transform.position, destination[destinationIndex].position) < distancespot)
        {
            if (waitTime <= 0)
            {
                chemin++;
                if (chemin >= destination.Length)
                {
                    chemin = 0;
                }
                destinationIndex = chemin;
                waitTime = startWaitTime;
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
        }

        // déplacement vers les points
        if (sentinnelalerte == false && searchPlayer == false && !isWaiting)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination[destinationIndex].position, speed * Time.deltaTime);
        }

        // déplacement vers le joueur
        else if (sentinnelalerte && playControl.cacher == false)
        {
            FollowPlayer();
        }
    }
    private void FollowPlayer()
    {
        AlerteTimer = 0;
        transform.position = player.transform.position;
        AppelEnnemis();
    }

    private void Attente()
    {
        if (isWaiting)
        {
           AlerteTimer += Time.deltaTime;
                
           if (AlerteTimer >= 10)
           {
               AlerteTimer = 0;
               searchPlayer = false;
               sentinnelalerte = false;
               isWaiting = false;
           }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && playControl.cacher == false)
        {
            sentinnelalerte = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && playControl.cacher == false)
        {
            sentinnelalerte = true;
            isWaiting = false;
        }
    }

//    private void SearchPlayer()
//    {
//        searchPlayer = true;
//        lastPos = transform.position;
//        posA = new Vector3(transform.position.x - dist, transform.position.y, transform.position.z);
//        posB = new Vector3(transform.position.x + dist, transform.position.y, transform.position.z);
//        distBetweenAB = Vector3.Distance(lastPos, posB);
//    }
}
