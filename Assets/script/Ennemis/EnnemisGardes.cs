﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class EnnemisGardes : MonoBehaviour
{
    private Checkpoint checkpoint;
    private PlayerController playerControl;
    private GameManager gameManager;
    
    public GameObject[] waypoint;
    private int indexWaypoint;
    private float WPradius = 1;
    
    public int speed;

    private GameObject player;
    public bool gardeAlerte;
    public bool isWaiting;
    public float timerAttente;
    public float timerTrous;
    
    public GameObject iconWarning;
    public GameObject iconSleep;

    private Vector3 direction;
    public bool moveHorizontal;
    
    
    // sauvegarder la position du player si il est détecté
    public bool savePosition;
    public Vector3 positionPlayerDetecte;


    void Start()
    {
        checkpoint = GameObject.FindGameObjectWithTag("spawner").GetComponent<Checkpoint>();
        
        player = GameObject.FindGameObjectWithTag("Player");
        playerControl = player.GetComponent<PlayerController>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        
        iconSleep.SetActive(false);
        iconWarning.SetActive(false);
    }

    void Update()
    {
        // sauvegarde de la position du player quand il est détecté et se cache
        if (gardeAlerte && !playerControl.cacher)
        {
            savePosition = true;

            if (savePosition)
            {
                positionPlayerDetecte = player.transform.position;
                savePosition = false;
            }
        }

        if (isWaiting)
        {
            gardeAlerte = false;
        }
        else if (gardeAlerte)
        {
            isWaiting = false;
        }
       
       FollowPlayer();
       DeplacementWP();
       Attente();
       DetectionTrous();
       RotationEnnemis();
    }
    
    
    //suis les WP
    private void DeplacementWP()
    {
        if (!gardeAlerte && !isWaiting) // si n'a pas repérer le joueur (ne cherche pas le joueur && n'a pas vus Poka)
        {
            timerAttente = 0;
            iconWarning.SetActive(false);
        
            if (Vector3.Distance(waypoint[indexWaypoint].transform.position, transform.position) < WPradius) // va au prochain waypoint
            {
                indexWaypoint++;
            
                // re boucle
                if (indexWaypoint >= waypoint.Length)
                {
                    indexWaypoint = 0;
                }
            }
            // déplacement vers un waypoint
            transform.position = Vector3.MoveTowards(transform.position, waypoint[indexWaypoint].transform.position,Time.deltaTime * speed);
        }
    }

    // suis le joueur
    private void FollowPlayer() // suis le joueur
    {
        if (gardeAlerte && !playerControl.cacher && !isWaiting) // si joueur n'est pas caché et a été vu
        {    
            positionPlayerDetecte = player.transform.position;
            
            iconWarning.SetActive(true);
            transform.position =  Vector3.MoveTowards(transform.position, player.transform.position, (speed * 2)* Time.deltaTime); // suis le joueur
    
            // calcul la distance entre Poka et l'ennemis
            float dist = Vector3.Distance(transform.position, player.transform.position);
            if (dist <= 1f)
            {
                gardeAlerte = false;
                checkpoint.TeleportSpawn();
                gameManager.target = player.transform.position; 
            }
        }
        
        // suis la dernière position du joueur
        else if (gardeAlerte && playerControl.cacher)
        {
            transform.position = Vector3.MoveTowards(transform.position, positionPlayerDetecte, speed * Time.deltaTime);
            if (transform.position == positionPlayerDetecte)
            {
                isWaiting = true;
            }
        }
    }

    private void Attente()
    {
        if (isWaiting) // si garde a vu Poka et Poka cacher
        {
            transform.position = transform.position;
            timerAttente += Time.deltaTime;
            
            if (timerAttente >= 3) // attend 3 secondes
            {
                timerAttente = 0;
                speed = 3;
                isWaiting = false;
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !playerControl.cacher)
        {
            gardeAlerte = true;
            isWaiting = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !playerControl.cacher)
        {
            gardeAlerte = true;
            isWaiting = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (gardeAlerte)
        {
            gardeAlerte = false;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player") && !playerControl.cacher)
        {
            checkpoint.TeleportSpawn();
        }
        
        if (other.gameObject.CompareTag("Player") && playerControl.cacher)
        {
            GetComponent<CapsuleCollider>().enabled = false;
            GetComponent<Rigidbody>().useGravity = false;
        }
        else
        {
            GetComponent<CapsuleCollider>().enabled = true;
            GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void DetectionTrous()
    {
        Ray trousRay = new Ray(transform.position + direction, Vector3.down);
        Debug.DrawRay(transform.position + direction, Vector3.down);
        RaycastHit trousHit;
        
        if (Physics.Raycast(trousRay, out trousHit,  2f)) // lance un raycast vers le sol 
        {
            speed = 3;
        }
        else
        {
            speed = 0;
            timerTrous += Time.deltaTime;
            
            if (timerTrous >= 3)
            {
                DeplacementWP();
                timerTrous = 0;
                speed = 3;

                if (gardeAlerte)
                {
                    speed = 0;
                }
                else
                {
                    DeplacementWP();
                    speed = 3;
                }
            }
        }
    }

    private void RotationEnnemis()
    {
        // se tourne dans la bonne direction
        if (!moveHorizontal)
        {
            transform.rotation = Quaternion.Euler(-90,90,0);
        }
        else
        {
            // rotation dans la direction du player
            if (gardeAlerte)
            {
                if (transform.position.x > player.transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(-90,90,0); // gauche
                    direction = new Vector3(-2f,0,0);
                }
                else if (transform.position.x < player.transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(-90,-90,0); // droite
                    direction = new Vector3(2f,0,0);
                }
            }
            //rotation dans le sens des WP
            else
            {
                if (transform.position.x > waypoint[indexWaypoint].transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(-90,0,90); // gauche
                    direction = new Vector3(-2f,0,0);
                }
                else if (transform.position.x <  waypoint[indexWaypoint].transform.position.x)
                {
                    transform.rotation = Quaternion.Euler(-90,0,-90); // droite
                    direction = new Vector3(2f,0,0);
                    
                }
            }
        }
    }
}
