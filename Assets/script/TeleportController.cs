﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;

public class TeleportController : MonoBehaviour
{
    private GameManager gameManager;
    
    Vector3 destination;
    GameObject player;
    public GameObject boutonTeleporteur;

    public GameObject sortie;

    void Start ()
    {
        player = GameObject.FindWithTag("Player");
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
	
    void Update ()
    {
 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(("Player")))
        {
            GameObject clone = Instantiate(boutonTeleporteur, transform.position, Quaternion.identity);
            clone.transform.parent = this.transform;
            clone.transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z - 2);
            clone.GetComponent<BoutonTeleporteur>().teleControl = GetComponent< TeleportController>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(("Player")))
        {
            Destroy(GameObject.FindWithTag("boutonteleporteur"));
        }
    }

    public void Teleportation()
    {
        player.transform.position = sortie.transform.position;
        gameManager.peutBouger = false;
    }
}
