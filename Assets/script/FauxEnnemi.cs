﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FauxEnnemi : MonoBehaviour
{
    public GameObject fauxEnnemi;
    public Rigidbody RBennemi;
    public float force;
    public bool entré;

    void Start()
    {
        entré = false;
    }

    void Update()
    {
        if(entré)
        RBennemi.transform.Translate(Vector3.right * Time.deltaTime * force);
    }
    private void OnTriggerExit(Collider other)
    {
        entré = true;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ennemis"))
        {
            fauxEnnemi.SetActive(false);
        }
    }
}
