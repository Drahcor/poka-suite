﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchelleController : MonoBehaviour{

    PlayerController playcontrol;

    Rigidbody rb;
    GameObject player;
    public GameObject boutonEchelle;

    void Start()
    {
        playcontrol = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        rb = GameObject.FindGameObjectWithTag("échelle").GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            GameObject clone = Instantiate(boutonEchelle, transform.position, Quaternion.identity);
            clone.transform.parent = this.transform;
            clone.transform.position = new Vector3(transform.position.x, transform.position.y +3, transform.position.z);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Destroy(GameObject.FindWithTag("bouton"));
    }
}
