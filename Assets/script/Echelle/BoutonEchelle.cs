﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoutonEchelle : MonoBehaviour{

    GameManager gameManager;
    Rigidbody rbPlayer;
    public bool playerPosition;

    Vector3 MoveX;

    GameObject player;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        rbPlayer = player.GetComponent<Rigidbody>();
    }

    void Update()
    {
        MoveX = player.transform.up * 100;

        //calcul de la position du joueur face a celle du bouton
        if(player.transform.position.y < this.gameObject.transform.position.y)
        {
            print("player sous le bouton");
            playerPosition = true;
        }
        else if (player.transform.position.y > this.gameObject.transform.position.y)
        {
            print("player sur le bouton");
            playerPosition = false;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            if (gameObject.tag == "BoutonEchelle" && playerPosition == true)
            {
                gameManager.bloque = true;
                rbPlayer.isKinematic = true ;
                player.transform.position = Vector3.MoveTowards(player.transform.position, MoveX, gameManager.speed * Time.deltaTime);
                //player.transform.Translate(Vector3.MoveTowards (player.transform.position, MoveX, playcontrol.speed * Time.deltaTime));
            }
        }

        if (Input.GetKeyUp(0))
        {
            gameManager.bloque = false;
            rbPlayer.isKinematic = false;
        }
    }

    private void OnMouseExit()
    {
        gameManager.bloque = false;
        rbPlayer.isKinematic = false;
    }

}
