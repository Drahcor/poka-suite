﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpTest : MonoBehaviour{

    public Transform starPos, endPos;
    public float speed = 5f;

    private float startTime, totalDistance;
    
    
    void Start()
    {
        startTime = Time.time;
        totalDistance = Vector3.Distance(starPos.position, endPos.position);
    }

  
    void Update()
    {
        float currentduration = (Time.time - startTime) * speed;
        float journeyFraction = currentduration / totalDistance;
        transform.position = Vector3.Lerp(starPos.position, endPos.position, journeyFraction);
    }
}
