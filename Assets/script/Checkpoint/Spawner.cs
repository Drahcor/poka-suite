﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
   public int valueIndex;
   private Renderer childRenderer;

   private void Start()
   {
      childRenderer = transform.GetChild(0).GetComponent<Renderer>();
   }

   private void OnTriggerEnter(Collider other)
   {
      if (other.CompareTag("Player"))
      {
         // pensser dans l'inspecteur des Spawner a rajouter dans indexSpawn le numéro du spawn !!!!!!!!!
         transform.parent.gameObject.GetComponent<Checkpoint>().indexSpawns = valueIndex;
         childRenderer.material.color = Color.yellow;
      }
   }
}
