﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour{

    public static Checkpoint Instance { private set; get; }
    private PlayerController playControl;
    private GameManager gameManager;
    
    private Scene scene;
    public Transform[] spawns;
    public int indexSpawns;

    GameObject player;
    public List<Transform> ObjetsTransform = new List<Transform>();
    public List<Vector3> Objetsposition = new List<Vector3>();


    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        scene = SceneManager.GetActiveScene();
        player = GameObject.FindGameObjectWithTag("Player");
        playControl = player.GetComponent<PlayerController>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        foreach (var ObjetTransform in ObjetsTransform)
        {
            Objetsposition.Add(ObjetTransform.position);
        }
        indexSpawns = PlayerPrefs.GetInt("Le nb de chexpoint parcourus est");
    }

    void Update()
    {
        if(Input.GetButton("Restart"))
        Restart();

        if (Input.GetKey(KeyCode.A))
        {
             TeleportSpawn();
        }
    }

    void Restart()
    {
        SceneManager.LoadScene(scene.name);
    }

    public void TeleportSpawn()
    {
        // pensser dans l'inspecteur des Spawner a rajouter dans indexSpawn le numéro du spawn !!!!!!!!!
        
       player.transform.position = new Vector3(spawns[indexSpawns].position.x,spawns[indexSpawns].position.y,playControl.positionXCheckpoint ); // teleportation au checkpoint de la liste
       for (int i = 0; i < ObjetsTransform.Count; i++) // incrémentation de indexSpawn
       {
           ObjetsTransform[i].position = Objetsposition[i];
           PlayerPrefs.SetInt("Le nb de chexpoint parcourus est" ,indexSpawns);
           gameManager.peutBouger = false;
       }
    }
}
