﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour{
    
    public bool cacher;

    private SphereRaycast sphereCast;

    public float positionXCheckpoint;

    private Renderer playerRenderer;
    private Color materiauDeBase;
  
    void Start ()
    {
        playerRenderer= transform.GetChild(2).GetChild(1).GetComponent<Renderer>();
        materiauDeBase = transform.GetChild(2).GetChild(1).GetComponent<Renderer>().material.color;
    }

    public void Update ()
    {
        // Player passe au travers des ennemis
//        Ray PlayerCacherray = new Ray(transform.position + gameManager.sensDesRaycast, gameManager.sensDesRaycast);
//        Debug.DrawRay(transform.position + gameManager.sensDesRaycast, gameManager.sensDesRaycast, Color.green);
//
//        RaycastHit PlayerCacherHit;
//
//        if (Physics.Raycast(PlayerCacherray, out PlayerCacherHit,1.5f))
//        {
//
//        }
        if (cacher)
        {
            playerRenderer.material.color = Color.black;
        }
        else
        {
            playerRenderer.material.color = materiauDeBase;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.CompareTag("cacher"))
            {
                cacher = true;
            }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("cacher"))
        {
            cacher = false;
        }
    }
}
