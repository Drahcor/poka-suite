﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{

    public Button play;
    public Button quit;
    public Button chooseLevel;

    public GameObject[] buttonLevel;

    void Start()
    {
        //bouton Level desactivés
        for (int i = 0; i < buttonLevel.Length; i++)
        {
            buttonLevel[i].SetActive(false);
        }
    }


    void Update()
    {
        
    }

    public void Play(int sceneIndex)
    {
        SceneManager.LoadScene (sceneIndex);
    }

    public void Leave()
    {
        Application.Quit ();
    }

    public void ChooseLevel()
    {
        // désactiver les premier boutons
        play.gameObject.SetActive(false);
        quit.gameObject.SetActive(false);
        chooseLevel.gameObject.SetActive(false);
        
        //activer les boutons level
        for (int i = 0; i < buttonLevel.Length; i++)
        {
            buttonLevel[i].SetActive(true);
        }
    }

    public void LevelOne(int sceneIndex)
    {
        SceneManager.LoadScene (sceneIndex+1);
    }

    public void LevelTwo(int sceneIndex)
    {
        SceneManager.LoadScene (sceneIndex+2);
    }
    
    public void LevelThree(int sceneIndex)
    {
        SceneManager.LoadScene (sceneIndex+3);
    }
}
