﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerCaisse : MonoBehaviour
{
    private GameManager gamemanager;
    public bool toucherGauche;
    public bool toucherDroite;
    
    
    void Start()
    {
        gamemanager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {    
        Ray caisseRayGauche = new Ray(transform.position, Vector3.left);
        Ray caisseRayDroite = new Ray(transform.position, Vector3.right);
        
        Debug.DrawRay(transform.position, Vector3.left, Color.red);
        Debug.DrawRay(transform.position, Vector3.right, Color.red);
        RaycastHit caisseHit;

        if (Physics.Raycast(caisseRayGauche, out caisseHit, 1f,gamemanager.groundLayer))
        {
            toucherGauche = true;
        }
        else if (Physics.Raycast(caisseRayDroite, out caisseHit, 1f, gamemanager.groundLayer))
        {
            toucherDroite = true;
        }
        else
        {
            toucherGauche = false;
            toucherDroite = false;
        }
    }
}
