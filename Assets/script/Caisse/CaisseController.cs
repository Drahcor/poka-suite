﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaisseController : MonoBehaviour
{
	private GameManager gameManager;

    Rigidbody rigidCaisse;
    
    public GameObject boutonCaisse;
    private GameObject clone;

    // déplacement caisse
    float directionX;
    Rigidbody rb;

    void Start()
    {
	    rigidCaisse = GetComponent<Rigidbody>();
	    rigidCaisse.constraints = RigidbodyConstraints.FreezeRotation;

	    gameManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GameManager>();

	    rb = GetComponent<Rigidbody>();
    }
    
	void Update ()
    {
	}
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("particule"))
		{
			Destroy(GameObject.FindWithTag("particule"));
		}
		
		if (other.gameObject.CompareTag(("Player")))
		{
			clone = Instantiate(boutonCaisse, transform.position,Quaternion.identity);
			clone.transform.parent = transform;
			clone.transform.position = new Vector3(transform.position.x, transform.position.y+2, transform.position.z);
		}   
	}

	private void OnCollisionStay(Collision other)
	{

	}

	private void OnTriggerExit(Collider other)
	{
        if (other.gameObject.CompareTag(("Player")))
        {
            Destroy(GameObject.FindWithTag("bouton"));
            gameManager.bloque = false;
        }
	}
}
