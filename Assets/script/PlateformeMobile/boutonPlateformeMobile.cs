﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boutonPlateformeMobile : MonoBehaviour {

    public Transform objetFollow;
    RectTransform thisRect;
    public RectTransform rectCanvas;
    Vector2 positionBoutonToScreen;

    public float moveX;
    public float moveY;
    public float moveZ;

    void Start()
    {
        this.gameObject.SetActive(false);
        thisRect = GetComponent<RectTransform>();
    }

    void Update()
    {
        GameObject boutonHaut = GameObject.Find("boutonPlateformeHaut");

        Vector2 positionBoutonWorld = objetFollow.transform.position + new Vector3(moveX, moveY, moveZ);
        positionBoutonToScreen = RectTransformUtility.WorldToScreenPoint(Camera.main, positionBoutonWorld);

        positionBoutonToScreen = new Vector2(
            positionBoutonToScreen.x - rectCanvas.sizeDelta.x / 2,
            positionBoutonToScreen.y - rectCanvas.sizeDelta.y / 2);
        thisRect.anchoredPosition = positionBoutonToScreen;
    }

    public void OnClickTask()
    {
        GameObject Plateforme = GameObject.Find("PorteLevé");

        Plateforme.transform.position = Plateforme.transform.position + new Vector3(0, 2f, 0);

    }
}
