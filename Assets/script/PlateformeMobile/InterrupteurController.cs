﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterrupteurController : MonoBehaviour
{
    public GameObject BoutonInterrupteur;
    public GameObject porte;
    
    
    
    private void OnTriggerExit(Collider other)
    {
      Destroy(GameObject.FindWithTag("bouton"));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == ("Player"))
        {
            GameObject clone = Instantiate(BoutonInterrupteur, transform.position, Quaternion.identity);
            clone.transform.parent = transform;
            clone.transform.position = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z-2);
        }
    }
}
