﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAppele : MonoBehaviour
{

    private PlayerController playerControl;
    private SphereRaycast sphereCast;
    private GameObject player;

    public int speed;
    public GameObject iconsleep;
    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        sphereCast = GameObject.FindGameObjectWithTag("RaycastMobile").GetComponent<SphereRaycast>();
        playerControl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }


    void Update()
    {
        //player détecté
        if (sphereCast.sentinnelalerte)
        {
            transform.position= Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * speed);
            iconsleep.SetActive(false);
        }
        else
        {
            iconsleep.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("échelle"))
        {
            Destroy(gameObject);
        }
    }
}
