﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cacher : MonoBehaviour
{
    private GameObject player;
    private PlayerController playercontrol;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playercontrol = player.GetComponent<PlayerController>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            playercontrol.cacher = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playercontrol.cacher = false;
        }
    }
}
