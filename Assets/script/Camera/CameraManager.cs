﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    CameraOffset cameraControl;

     GameObject camera;

    void Start()
    {
        cameraControl = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraOffset>();
        camera= GameObject.FindGameObjectWithTag("MainCamera");
    }


    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("grotte");
            cameraControl.offset = cameraControl.offset - new Vector3(0,2f,0);
            camera.transform.rotation = Quaternion.Euler(0,0,0);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        cameraControl.offset =  cameraControl.offset + new Vector3(0,2f,0);
    }
}
