﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOffet : MonoBehaviour
{
    private CameraOffset cameraOffset;
    
    // entrer un nombre dans Unity pour décider du choix de l'offet dans le code CameraOffset
    public int IndexRotation;
    void Start()
    {
        cameraOffset = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraOffset>();
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {             
            // boolean présent sur la camera qui lui autorise à changer de position
            cameraOffset.cameraMove = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {             
            // boolean présent sur la camera qui lui autorise à changer de position
            cameraOffset.cameraMove = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        cameraOffset.cameraMove = false;
    }
}
