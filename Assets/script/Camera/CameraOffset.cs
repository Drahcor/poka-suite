﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;

public class CameraOffset : MonoBehaviour
{
    private TriggerOffet choixRotation;
    PlayerController playcontrol;
    Checkpoint gameManager;
    GameObject player;
    
    public Vector3 offset;
    public Vector3 offsetPanique;
    // ce met sur offset grotte si TriggerOffset est sur 1
    public Vector3 offsetGrotte;
    //offset 2 = en hauteur
    public Vector3 offsetHauteur;

    public bool cameraMove;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playcontrol = player.GetComponent<PlayerController>();
        //choixRotation = GameObject.FindGameObjectWithTag("cameraRotation").GetComponent<TriggerOffet>();
        cameraMove = false;

        gameManager = GameObject.FindGameObjectWithTag("spawner").GetComponent<Checkpoint>();
    }

    void Update()
    {
      Grotte();
      Hauteur();
      transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
      
      
    }
    private void Grotte()
    {
        if (cameraMove && choixRotation.IndexRotation ==1)
        {
            print("1");
            transform.rotation = Quaternion.Euler(10,0,0);
            transform.position = player.transform.position + offsetGrotte;
        }
    }

    private void Hauteur()
    {
        if (cameraMove && choixRotation.IndexRotation == 2)
        {
            print("2");
            transform.position = player.transform.position + offsetHauteur;
        }
    }

    public void ReculerCamera()
    {
        if (Input.GetMouseButtonUp(0))
        {
            offset = offset - new Vector3(0,0,1);
        }
    }

    public void AvancerCamera()
    {
        if (Input.GetMouseButtonUp(0))
        {
            offset = offset + new Vector3(0,0,1);
        }
    }

    public void Respawn()
    {
        if (Input.GetMouseButtonUp(0))
        {
            gameManager.TeleportSpawn();
        }
    }

}
